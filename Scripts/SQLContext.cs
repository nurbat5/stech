﻿using Microsoft.EntityFrameworkCore;

namespace STehnology.Scripts
{
    public abstract class SQLContext : DbContext
    {
        public readonly string tableName;
        protected SQLContext(string tableName)
        {
            this.tableName = tableName;
        }
        
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite(new SQLiteConfiguration.SQLiteConnection(tableName).GetConnection());
        }
    }
}