﻿namespace STehnology.Scripts
{
    public class EnemyChildrens
    {
        public string ParentEnemyId { get; set; }
        public string ChildrenEnemyId { get; set; }
    }
}