﻿using System;

namespace STehnology.Scripts
{
    // Есть 3 группы сотрудников - Employee, Manager и Salesman.
    public class GroupCompany
    {
        public string Id { get; set; } = Guid.NewGuid().ToString();
        public string NameGroup { get; set; }
        
        //это базовая ставка плюс N% но не больше N% суммарной надбавки за стаж работы.
        public float PayPlex { get; set; }
        public float MaxPlex { get; set; }
        
        // Плюс 0,5% зарплаты всех подчинённых первого уровня.
        public float EnemyPlex { get; set; } = 0;

        public static void InjectSql()
        {
            SQLite.Instance.AddRangeAsync(new GroupCompany[]
            {
                new GroupCompany()
                {
                    NameGroup = "Employee",
                    PayPlex = 3,
                    MaxPlex = 30
                },
                new GroupCompany()
                {
                    NameGroup = "Manager",
                    PayPlex = 5,
                    MaxPlex = 40,
                    EnemyPlex = 0.5f
                },
                new GroupCompany()
                {
                    NameGroup = "Salesman",
                    PayPlex = 1,
                    MaxPlex = 35,
                    EnemyPlex = 0.3f
                },
            });
            SQLite.Instance.SaveChangesAsync();
        }
    }
}