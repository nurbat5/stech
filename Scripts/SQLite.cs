﻿using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace STehnology.Scripts
{
    public sealed class SQLite : SQLContext
    {
        private static SQLite _sqLite;
        public static SQLite Instance => _sqLite ??= new SQLite();
        
        public DbSet<EnemyCompany> enemysCompany { get; set; }
        public DbSet<GroupCompany> groupsCompany { get; set; }
        // public DbSet<EnemyChildrens> enemyChildrens { get; set; }

        public SQLite() : base("STech.DataBase")
        {
            _sqLite = this;
            Initilize();
        }

        private async void Initilize()
        {
            if (await Database.EnsureCreatedAsync())
            {
                GroupCompany.InjectSql();
                EnemyCompany.InjectSql();
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            
            modelBuilder.Entity<EnemyCompany>().HasKey(k => k.Id);
            modelBuilder.Entity<GroupCompany>().HasKey(k => k.Id);
            // modelBuilder.Entity<EnemyChildrens>();
        }
    }
}