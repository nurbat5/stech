﻿using System.Data.Common;
using Microsoft.Data.Sqlite;

namespace STehnology.Scripts
{
    public class SQLiteConfiguration
    {
        public sealed class SQLiteConnection : SqliteConnectionStringBuilder
        {
            public SQLiteConnection(string localDb)
            {
                DataSource = localDb;
                Password = string.Empty;
                RecursiveTriggers = false;
                Cache = SqliteCacheMode.Default;
                ForeignKeys = true;
                Mode = SqliteOpenMode.ReadWriteCreate;
            }
        
            public string GetConnectionString() => ConnectionString;
            public DbConnection GetConnection() => new SqliteConnection(GetConnectionString());
        }

    }
}