using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Chromely;
using Chromely.CefGlue.Browser;
using Chromely.Core;
using Chromely.Core.Configuration;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Xilium.CefGlue;

namespace STehnology
{
    public class Program
    {
        private static Mutex mutex = new Mutex(true, "{8F6F0AC4-B9A1-45fd-A8CF-72F04E6BDE8F}");
        
        [STAThread]
        public static void Main(string[] args)
        {
            var proctype = ClientAppUtils.GetProcessType(args);
            if (proctype == ProcessType.Browser)
            {
                if (mutex.WaitOne(TimeSpan.Zero, true))
                {
                    CreateHostBuilder(args).Build().RunAsync();
                    Initilize(args);
                    mutex.ReleaseMutex();
                }
                else Environment.Exit(0);
            }
            else
            {
                // Child processes will run here
                Initilize(args);
            }
            
            CreateHostBuilder(args).Build().Run();
        }
        
        private static void Initilize(string[] args)
        {
            var config = DefaultConfiguration.CreateForRuntimePlatform();
            config.StartUrl = "localhost:5000";
            // config.StartUrl = "https://google.com";
            // config.StartUrl = "local://app/index.html";
            config.WindowOptions.Title = "Vodji Chat";
            config.DebuggingMode = false;
            AppBuilder.Create()
                .UseConfiguration<DefaultConfiguration>(config)
                .UseApp<STechApp>()
                .Build()
                .Run(args);
        }

        private sealed class STechApp : ChromelyBasicApp { }
        


        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                    webBuilder.UseUrls(new[]
                    {
                        "http://localhost:5000"
                    });
                });
    }
}